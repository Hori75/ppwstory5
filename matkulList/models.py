import re
from django.utils.translation import gettext_lazy as _

from django import forms
from django.db import models

termpattern = re.compile(r"^\b(Gasal|Genap) \d{4}\/\d{4}$")
locpattern1 = re.compile(r"^\d{1}\.\d{4}$")
locpattern2 = re.compile(r"^([A-Z])\d{1}\.\d{2}$")

class MatKul(models.Model):
    title = models.CharField(max_length=100)
    term = models.CharField(max_length=15)
    lecturer = models.CharField(max_length=100)
    total_credits = models.IntegerField()
    location = models.CharField(max_length=50)
    description = models.TextField(max_length=2000)

    def __str__(self):
        return self.title



class MatKulForm(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = ['title', 'term', 'lecturer', 'total_credits', 'location', 'description']
        labels = {
            'total_credits' : _('Total credits')
        }

    def clean(self):
        super(MatKulForm, self).clean()

        term = self.cleaned_data.get('term')

        if (not termpattern.match(term)):
            self._errors['term'] = self.error_class(
                ['Incorrect term format! (Should be "Gasal 2019/2020" or "Genap 2019/2020" in any year)']
            )

        return self.cleaned_data
        


