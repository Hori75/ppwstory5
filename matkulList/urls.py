from django.urls import  path

from . import views

app_name = 'matkulList'

urlpatterns = [
    path('', views.index, name="index"),
    path('add/', views.add, name="add"),
    path('view/<int:id>', views.view, name="view"),
    path('delete/<int:id>', views.remove, name="delete")
]
