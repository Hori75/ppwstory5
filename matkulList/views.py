from .models import MatKul, MatKulForm
from django.contrib.messages import get_messages, success, error
from django.http import HttpResponseBadRequest
from django.shortcuts import get_object_or_404, redirect, render, reverse

def index(request):
    messages = get_messages(request)
    matkulList = MatKul.objects.all()
    return render(request, 'matkulList/index.html', {'matkulList' : matkulList, "messages" : messages})

def view(request, id):
    matkul = MatKul.objects.filter(pk=id).first()
    if (matkul == None):
        error(request, "Matkul tidak ditemukan!")
        return redirect(reverse("matkulList:index"))
    return render(request, 'matkulList/view.html', {'matkul' : matkul})

def add(request):
    if (request.method == "POST"):
        form = MatKulForm(request.POST)
        if (form.is_valid()):
            matkul = MatKulForm.save(form)
            success(request, "Matkul telah ditambahkan!")
            return redirect(reverse('matkulList:index'))
        else:
            return render(request, 'matkulList/add.html', {'form' : form})
    else:
        form = MatKulForm()
    
    return render(request, 'matkulList/add.html', {'form' : form})

def remove(request, id):
    matkul = MatKul.objects.filter(pk=id).first()
    if (matkul == None):
        error(request, "Matkul tidak ditemukan!")
        return redirect(reverse("matkulList:index"))
    if (request.method == "POST"):
        MatKul.delete(matkul)
        success(request, "Matkul telah dihapus!")
        return redirect(reverse("matkulList:index"))
    else:
        return render(request, 'matkulList/remove.html', {'matkul' : matkul})